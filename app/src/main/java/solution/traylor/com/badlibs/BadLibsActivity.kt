package solution.traylor.com.badlibs

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import solution.traylor.com.badlibs.data.Classification

class BadLibsActivity : AppCompatActivity() {

    private lateinit var classifications: Array<Classification>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_badlibs)

        classifications = Classification.values()
    }

}