package solution.traylor.com.badlibs.koin

import androidx.room.Room
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import solution.traylor.com.badlibs.App
import solution.traylor.com.badlibs.data.WordRepository
import solution.traylor.com.badlibs.data.WordRoomDatabase
import solution.traylor.com.badlibs.viewmodel.BadLibsViewModel

val koinModule = module {
    viewModel { BadLibsViewModel(get()) }
    single { WordRepository(App.application) }
}