package solution.traylor.com.badlibs.data

import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.Room
import androidx.sqlite.db.SupportSQLiteDatabase
import solution.traylor.com.badlibs.R


@Database(entities = [Word::class], version = 1)
abstract class WordRoomDatabase : RoomDatabase() {
    abstract fun wordDao(): WordDao

    companion object {
        @Volatile
        private var INSTANCE: WordRoomDatabase? = null

        fun getInstance(context: Context): WordRoomDatabase = INSTANCE ?: synchronized(this) {
            INSTANCE?:buildDatabase(context).also { INSTANCE = it }
        }

        private fun buildDatabase(context: Context): WordRoomDatabase {
            return Room.databaseBuilder(context.applicationContext,
                    WordRoomDatabase::class.java, "word_database")
                    .addCallback(RoomDatabaseCallback(context))
                    .build()
        }

        private class RoomDatabaseCallback(val context : Context) : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let {
                    doAsync{ populateDbAsync(it, context) }.execute()
                }
            }
        }

        class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {
            override fun doInBackground(vararg params: Void?): Void? {
                handler()
                return null
            }
        }

        private fun populateDbAsync(wordDaoDbAsync: WordRoomDatabase, context: Context) {
            var dao = wordDaoDbAsync.wordDao()

            addToDb(R.array.adjective, Classification.ADJECTIVE, dao, context)
            addToDb(R.array.adverb, Classification.ADVERB, dao, context)
            addToDb(R.array.noun, Classification.NOUN, dao, context)
            addToDb(R.array.pronouns, Classification.PRONOUN, dao, context)
            addToDb(R.array.verb, Classification.VERB, dao, context)
        }

        private fun addToDb(resourceIdentifier: Int, classification: Classification, dao: WordDao, context: Context) {
            for (word in context.resources.getStringArray(resourceIdentifier)) {
                dao.insert(Word(word, classification))
            }
        }
    }
}