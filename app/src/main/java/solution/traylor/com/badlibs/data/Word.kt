package solution.traylor.com.badlibs.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

@Entity(tableName = "word_table")
@TypeConverters(Converters::class)
class Word(@PrimaryKey val value: String, val classification: Classification)

@Entity
enum class Classification {
    ADJECTIVE,
    ADVERB,
    GREETING,
    NOUN,
    NUMBER,
    PRONOUN,
    EXCLAMATION,
    VERB
}