package solution.traylor.com.badlibs.data

import android.app.Application
import androidx.lifecycle.LiveData
import android.os.AsyncTask
import android.util.Log

class WordRepository(application: Application) {
    private var wordDao: WordDao

    init {
        val db = WordRoomDatabase.getInstance(application)
        wordDao = db.wordDao()
    }

    fun getAllWords(): LiveData<List<Word>> {
        return wordDao.getAllWords()
    }

    fun getWordsByClassification(classification : Classification): LiveData<List<Word>> {
        return wordDao.getWordsByClassification(classification)
    }

    fun insert(word: Word) {
        Log.d("tag", "Inserting the value ${word.value}")
        InsertAsyncTask(wordDao).execute(word)
    }

    class InsertAsyncTask internal constructor(private val asyncTaskDao: WordDao) : AsyncTask<Word, Void, Void>() {
        override fun doInBackground(vararg params: Word): Void? {
            asyncTaskDao.insert(params[0])
            return null
        }
    }
}