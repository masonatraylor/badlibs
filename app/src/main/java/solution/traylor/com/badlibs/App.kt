package solution.traylor.com.badlibs

import android.app.Application
import org.koin.android.ext.android.startKoin
import solution.traylor.com.badlibs.koin.koinModule

class App : Application() {

    init {
        application = this
    }

    override fun onCreate(){
        super.onCreate()
        // start Koin!
        startKoin(this, listOf(koinModule))
    }

    companion object {
        lateinit var application : App
        private set
    }
}