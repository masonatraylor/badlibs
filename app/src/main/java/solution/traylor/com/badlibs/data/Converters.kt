package solution.traylor.com.badlibs.data

import androidx.room.TypeConverter

class Converters {

    @TypeConverter
    fun toString(classification: Classification): String {
        return classification.toString()
    }

    @TypeConverter
    fun toClassification(stringValue: String): Classification {
        return Classification.valueOf(stringValue)
    }
}