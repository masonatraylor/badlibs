package solution.traylor.com.badlibs.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.TypeConverters

@Dao
@TypeConverters(Converters::class)
interface WordDao {

    @Insert(onConflict = REPLACE)
    fun insert(word: Word) : Long

    @Query("DELETE FROM word_table")
    fun deleteAll()

    @Query("SELECT * FROM word_table")
    fun getAllWords() : LiveData<List<Word>>

    @Query("SELECT * FROM word_table WHERE classification=:classification")
    fun getWordsByClassification(classification: Classification) : LiveData<List<Word>>
}