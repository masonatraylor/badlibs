package solution.traylor.com.badlibs.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import solution.traylor.com.badlibs.data.Classification
import solution.traylor.com.badlibs.databinding.ListItemClassificationBinding

/**
 * Adapter for the [RecyclerView] in [PlantListFragment].
 */
class MyAdapter : ListAdapter<Classification, MyAdapter.ViewHolder>(ClassificationDiffCallback()) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val classification = getItem(position)
        holder.apply {
            bind(createOnClickListener(classification), classification)
            itemView.tag = classification
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemClassificationBinding.inflate(
                LayoutInflater.from(parent.context), parent, false))
    }

    private fun createOnClickListener(classification: Classification): View.OnClickListener {
        return View.OnClickListener {
            Log.d("click tag", "${classification.toString()} got clicked!")
//            val direction = PlantListFragmentDirections.ActionPlantListFragmentToPlantDetailFragment(plantId)
//            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
            private val binding: ListItemClassificationBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: Classification) {
            binding.apply {
                clickListener = listener
                classification = item
                executePendingBindings()
            }
        }
    }
}