package solution.traylor.com.badlibs.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import kotlinx.android.synthetic.main.fragment_sentence.*
import org.koin.android.viewmodel.ext.android.sharedViewModel
import solution.traylor.com.badlibs.R
import solution.traylor.com.badlibs.data.Classification
import solution.traylor.com.badlibs.data.Word
import solution.traylor.com.badlibs.viewmodel.BadLibsViewModel

class SentenceFragment : Fragment() {

    private val vm : BadLibsViewModel by sharedViewModel()

    private var words : List<Word> = ArrayList()

    private lateinit var navController : NavController

    var classificationMap : MutableMap<Classification, List<Word>> = HashMap()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        return inflater.inflate(R.layout.fragment_sentence, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeLiveData()

        buttonRefresh.setOnClickListener {
            generateNewSentence()
        }
    }

    fun getRandomElement(list : List<Word>?) : Word {
        if (list == null || list.isEmpty()) {
            return Word("lol wut", Classification.GREETING)
        }
        return list.get(getRandomInt(list.size))
    }

    fun getRandomInt(maximum : Int) : Int {
        return (Math.random() * maximum).toInt()
    }

    fun observeLiveData() {
        vm.getAllWords().observe(this, Observer {
            it?.let {
                words = it
                generateNewSentence()
            }
        })

        for (classification in Classification.values()) {
            vm.getWordsByClassification(classification).observe(this, Observer {
                it?.let {
                    classificationMap.put(classification, it)
                }
            })
        }
    }

    fun generateNewSentence() {
        textPronoun1.text = getWordOfClassification(Classification.PRONOUN)
        textAdverb.text = getWordOfClassification(Classification.ADVERB)
        textVerb.text = getWordOfClassification(Classification.VERB)
        textPronoun2.text = getWordOfClassification(Classification.PRONOUN) + "'s"
        textAdjective.text = getWordOfClassification(Classification.ADJECTIVE)
        textNoun.text = getWordOfClassification(Classification.NOUN)
    }

    fun getWordOfClassification(classification: Classification) : String {
        return getRandomElement(classificationMap.get(classification)).value
    }
}