package solution.traylor.com.badlibs.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import org.koin.android.viewmodel.ext.android.sharedViewModel
import solution.traylor.com.badlibs.adapters.MyAdapter
import solution.traylor.com.badlibs.data.Classification
import solution.traylor.com.badlibs.databinding.FragmentClassificationListBinding
import solution.traylor.com.badlibs.databinding.ListItemClassificationBinding
import solution.traylor.com.badlibs.viewmodel.BadLibsViewModel

class ClassificationListFragment : Fragment() {

    private val vm by sharedViewModel<BadLibsViewModel>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        val binding = FragmentClassificationListBinding.inflate(inflater, container, false)
        val context = context ?: return binding.root

        val adapter = MyAdapter()
        adapter.submitList(Classification.values().toMutableList())
        binding.classificationList.adapter = adapter
        subscribeUi(adapter)

        setHasOptionsMenu(true)
        return binding.root
    }

    fun subscribeUi(adapter: MyAdapter) {
        vm.getAllWords()
    }
}