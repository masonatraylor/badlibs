package solution.traylor.com.badlibs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import solution.traylor.com.badlibs.data.Classification
import solution.traylor.com.badlibs.data.Word
import solution.traylor.com.badlibs.data.WordRepository

class BadLibsViewModel(private val repo: WordRepository) : ViewModel() {

    fun getAllWords() : LiveData<List<Word>> {
        return repo.getAllWords()
    }

    fun getWordsByClassification(classification: Classification) : LiveData<List<Word>> {
        return repo.getWordsByClassification(classification)
    }

}