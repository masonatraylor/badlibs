package solution.traylor.com.badlibs.adapters

import androidx.recyclerview.widget.DiffUtil
import solution.traylor.com.badlibs.data.Classification

class ClassificationDiffCallback : DiffUtil.ItemCallback<Classification>() {

    override fun areContentsTheSame(oldItem: Classification, newItem: Classification): Boolean {
        return oldItem.equals(newItem)
    }

    override fun areItemsTheSame(oldItem: Classification, newItem: Classification): Boolean {
        return oldItem.equals(newItem)
    }

}