package solution.traylor.com.badlibs.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.Navigation
import kotlinx.android.synthetic.main.fragment_badlibs_homescreen.*
import solution.traylor.com.badlibs.R

class BadLibsHomeScreenFragment : Fragment() {

    private lateinit var navController : NavController

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_badlibs_homescreen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        navController = Navigation.findNavController(view)
        setupButtons()
    }

    private fun setupButtons() {
        buttonShop.setOnClickListener { }
        buttonPlay.setOnClickListener {
            navController.navigate(R.id.action_badLibsHomeScreen_to_sentenceFragment)
        }
        buttonWordPacks.setOnClickListener {
            navController.navigate(R.id.action_badLibsHomeScreen_to_wordPackFragment)
        }
    }
}
